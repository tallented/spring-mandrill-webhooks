/**
 * Copyright (c) 2013  Forty Amp Software <http://www.40amps.com>
 *
 *  This file is part of the Spring-Mandrill-Webhooks project:
 * (https://bitbucket.org/FortyAmpSoftware/spring-mandrill-webhooks/overview)
 *
 *  Spring-Mandrill-Webhooks is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Spring-Mandrill-Webhooks is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser Public License
 *  along with Spring-Mandrill-Webhooks.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.fortyampsoftware.springmandrillwebhook.model.mandrill;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * A Mandrill Open model object used to contain Mandrill Webhook data.
 * 
 * See <a href="http://help.mandrill.com/entries/24466132-Webhook-Format" target="_new">http://help.mandrill.com/entries/24466132-Webhook-Format</a> for details.
 *
 * @author D. Scott Felblinger
 */
public class Open implements Serializable
{

	private static final long serialVersionUID = -1225231446173937213L;

	@JsonProperty("ts")
	private Long timestamp;

    private String ip;

    private String ua;

    private String location;

	/**
	 * @return the timestamp
	 */
	public Long getTimestamp()
	{
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Long timestamp)
	{
		this.timestamp = timestamp;
	}

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUa() {
        return ua;
    }

    public void setUa(String ua) {
        this.ua = ua;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Open open = (Open) o;

        if (!ip.equals(open.ip)) return false;
        if (!timestamp.equals(open.timestamp)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = timestamp.hashCode();
        result = 31 * result + ip.hashCode();
        return result;
    }
}
