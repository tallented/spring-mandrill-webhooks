/**
 * Copyright (c) 2013  Forty Amp Software <http://www.40amps.com>
 *
 *  This file is part of the Spring-Mandrill-Webhooks project:
 * (https://bitbucket.org/FortyAmpSoftware/spring-mandrill-webhooks/overview)
 *
 *  Spring-Mandrill-Webhooks is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Spring-Mandrill-Webhooks is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser Public License
 *  along with Spring-Mandrill-Webhooks.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.fortyampsoftware.springmandrillwebhook.model.mandrill;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

/**
 * A Mandrill EventData model object used to contain Mandrill Webhook data.
 * 
 * See <a href="http://help.mandrill.com/entries/24466132-Webhook-Format" target="_new">http://help.mandrill.com/entries/24466132-Webhook-Format</a> for details.
 * @author D. Scott Felblinger
 *
 */
public class EventData implements Serializable
{
	private static final long serialVersionUID = -4654481962932552309L;

	private Event event;

    @JsonProperty("_id")
    private String id;

	@JsonProperty("msg")
	private Message message;
	
	@JsonProperty("ts")
	private Long timestamp;
	private String ip;
	private Location location;
	
	@JsonProperty("user_agent")
	private String userAgent;
	
	@JsonProperty("user_agent_parsed")
	private UserAgent userAgentParsed;
	private String url;
	
	public enum Event
	{
		SEND,
		HARD_BOUNCE,
		OPEN,
		SPAM,
		REJECT,
		SOFT_BOUNCE,
		CLICK,
		UNSUB,
		DEFERRAL;
		
		@JsonValue
		public String toJson()
		{
			return name().toLowerCase();
		}
		
		@JsonCreator
		public static Event fromJson(String text)
		{
			return valueOf(text.toUpperCase());
		}
	}
	
	
	/**
	 * @return the event
	 */
	public String getEventAsString()
	{
		return event.toString();
	}
	
	/**
	 * @return the event
	 */
	public Event getEvent()
	{
		return event;
	}
	
	/**
	 * @param event the event to set
	 */
	public void setEvent(Event event)
	{
		this.event = event;
	}

	/**
	 * @return the timestamp
	 */
	public Long getTimestamp()
	{
		return timestamp;
	}
	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Long timestamp)
	{
		this.timestamp = timestamp;
	}
	/**
	 * @return the ip
	 */
	public String getIp()
	{
		return ip;
	}
	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip)
	{
		this.ip = ip;
	}
	/**
	 * @return the location
	 */
	public Location getLocation()
	{
		return location;
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(Location location)
	{
		this.location = location;
	}
	/**
	 * @return the message
	 */
	public Message getMessage()
	{
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(Message message)
	{
		this.message = message;
	}
	/**
	 * @return the userAgent
	 */
	public String getUserAgent()
	{
		return userAgent;
	}
	/**
	 * @param userAgent the userAgent to set
	 */
	public void setUserAgent(String userAgent)
	{
		this.userAgent = userAgent;
	}
	/**
	 * @return the userAgentParsed
	 */
	public UserAgent getUserAgentParsed()
	{
		return userAgentParsed;
	}
	/**
	 * @param userAgentParsed the userAgentParsed to set
	 */
	public void setUserAgentParsed(UserAgent userAgentParsed)
	{
		this.userAgentParsed = userAgentParsed;
	}
	/**
	 * @return the url
	 */
	public String getUrl()
	{
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url)
	{
		this.url = url;
	}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
