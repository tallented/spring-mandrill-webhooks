/**
 * Copyright (c) 2013  Forty Amp Software <http://www.40amps.com>
 *
 *  This file is part of the Spring-Mandrill-Webhooks project:
 * (https://bitbucket.org/FortyAmpSoftware/spring-mandrill-webhooks/overview)
 *
 *  Spring-Mandrill-Webhooks is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Spring-Mandrill-Webhooks is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser Public License
 *  along with Spring-Mandrill-Webhooks.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.fortyampsoftware.springmandrillwebhook.controller;

import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.fortyampsoftware.springmandrillwebhook.service.MandrillKeyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fortyampsoftware.springmandrillwebhook.domain.AuthKeyPropertyKey;
import com.fortyampsoftware.springmandrillwebhook.domain.WebhookUrlPropertyKey;
import com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events;
import com.fortyampsoftware.springmandrillwebhook.service.MandrillSignatureValidationService;

/**
 * AuthenticatingMandrillWebhookController is the controller used to receive, validate and route 
 * incoming Mandrill Webhook callbacks in POST form.  Each request mapping has been created to handle a specific 
 * event type as defined in Mandrill's webhook documentation: 
 * <a href="http://help.mandrill.com/entries/21738186-Introduction-to-Webhooks" target="_new">http://help.mandrill.com/entries/21738186-Introduction-to-Webhooks</a>
 * <br />
 * The AuthenticatingMandrillWebhookController will utilize the MandrillSignatureValidationService 
 * to validate the X-Mandrill-Signature header received with each Mandrill POST request.  If the signature 
 * header is not valid, the controller will respond with an HttpStatus.UNAUTHORIZED response.
 * Mandrill documents regarding authenticating webhooks can be found here:
 * <a href="http://help.mandrill.com/entries/23704122-Authenticating-webhook-requests" target="_new">http://help.mandrill.com/entries/23704122-Authenticating-webhook-requests</a>
 * <br />
 * 
 * This class will also support and send HttpStatus.OK responses to HEAD requests in order to 
 * handle the setup of authorized webhook requests. 
 * See: <a href="http://help.mandrill.com/entries/22024856-Why-can-t-my-webhook-or-inbound-route-URL-be-verified-" target="_new">http://help.mandrill.com/entries/22024856-Why-can-t-my-webhook-or-inbound-route-URL-be-verified-</a>
 * for details.
 * 
 * @author D. Scott Felblinger
 *
 */
@Controller
@RequestMapping("/secure")
public class AuthenticatingMandrillWebhookController extends MandrillWebhookController
{
	private static final Logger log = LoggerFactory.getLogger(AuthenticatingMandrillWebhookController.class);
	
	/**
	 * The service used to validate a given signature from Mandrill request against a 
	 * given authorization key.
	 */
	@Autowired
	private MandrillSignatureValidationService mandrillSignatureValidationService;

	/**
	 * Properties object containing the values for authentication keys and webhook urls injected
	 * from the spring-mandrill-webhook.properties file.
	 */
    @Autowired
    private MandrillKeyService mandrillKeyService;
	
	
	/**
	 * Catch-all mapping for handling HEAD requests to this controller.
	 */
	@Override
	@RequestMapping(value={"/multi","send","hard-bounce","opened","spam","rejected","delayed","soft-bounce","click","unsubscribe"},method=RequestMethod.HEAD)
	public ResponseEntity<String>processAnyHeadRequest(HttpServletRequest request)
	{
		log.debug("Recieved HEAD request for {}"+request.getContextPath()+request.getServletPath());
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	
	/**
	 * Handles the Mandrill Multi Webhook callback with signature authentication.
	 * @param events data instantiated from parsed JSON on Mandrill callback.
	 * @param request
	 * @return HttpStatus.OK if the X-Mandrill-Signature header matches the expected value.  If not
	 * HttpStatus.UNAUTHORIZED is returned.
	 */
	@Override
	@RequestMapping(value="/multi",method={RequestMethod.POST},consumes="application/x-www-form-urlencoded")
	public ResponseEntity<String> processAnyRequest(@RequestParam(value="mandrill_events") Events events, HttpServletRequest request) 
	{
		boolean validSignature = mandrillSignatureValidationService.verifySignature(request, 
																	mandrillKeyService.getProperty(AuthKeyPropertyKey.MANDRILL_AUTH_KEY_MULTI),
																	mandrillKeyService.getProperty(WebhookUrlPropertyKey.MANDRILL_WEBHOOK_URL_MULTI));
		
		if(validSignature)
		{
			return super.processAnyRequest(events, request);
		}
		else
		{
			logAuthWarning(request.getContextPath()+request.getServletPath());
			return new ResponseEntity<String>(HttpStatus.UNAUTHORIZED);
		}
	}
	
	
	/**
	 * Handles the Mandrill Sent Webhook callback with signature authentication.  
	 * @param events data instantiated from parsed JSON of Mandrill callback.
	 * @param request
	 * @return HttpStatus.OK if the X-Mandrill-Signature header matches the expected value.  If not
	 * HttpStatus.UNAUTHORIZED is returned.
	 */
	@Override
	@RequestMapping(value="/send",method={RequestMethod.POST},consumes="application/x-www-form-urlencoded")
	public ResponseEntity<String> processSend(@RequestParam(value="mandrill_events") Events events, HttpServletRequest request) 
	{
		boolean validSignature = mandrillSignatureValidationService.verifySignature(request, 
																	mandrillKeyService.getProperty(AuthKeyPropertyKey.MANDRILL_AUTH_KEY_SEND),
																	mandrillKeyService.getProperty(WebhookUrlPropertyKey.MANDRILL_WEBHOOK_URL_SEND));
		
		if(validSignature)
			return super.processSend(events, request);
		else
		{
			logAuthWarning(request.getContextPath()+request.getServletPath());
			return new ResponseEntity<String>(HttpStatus.UNAUTHORIZED);
		}
	}
	

	/**
	 * Handles the Mandrill Hard Bounce Webhook callback with signature authentication.  
	 * @param events data instantiated from parsed JSON of Mandrill callback.
	 * @param request
	 * @return HttpStatus.OK if the X-Mandrill-Signature header matches the expected value.  If not
	 * HttpStatus.UNAUTHORIZED is returned.
	 */
	@Override
	@RequestMapping(value="/hard-bounce",method={RequestMethod.POST},consumes="application/x-www-form-urlencoded")
	public ResponseEntity<String> processHardBounce(@RequestParam(value="mandrill_events") Events events, HttpServletRequest request) 
	{
		boolean validSignature = mandrillSignatureValidationService.verifySignature(request, 
																	mandrillKeyService.getProperty(AuthKeyPropertyKey.MANDRILL_AUTH_KEY_HARD_BOUNCE),
																	mandrillKeyService.getProperty(WebhookUrlPropertyKey.MANDRILL_WEBHOOK_URL_HARD_BOUNCE));

		if(validSignature)
			return super.processHardBounce(events, request);
		else
		{
			logAuthWarning(request.getContextPath()+request.getServletPath());
			return new ResponseEntity<String>(HttpStatus.UNAUTHORIZED);
		}
	}

	

	/**
	 * Handles the Mandrill Opened Webhook callback with signature authentication.  
	 * @param events data instantiated from parsed JSON of Mandrill callback.
	 * @param request
	 * @return HttpStatus.OK if the X-Mandrill-Signature header matches the expected value.  If not
	 * HttpStatus.UNAUTHORIZED is returned.
	 */
	@Override
	@RequestMapping(value="/opened",method={RequestMethod.POST},consumes="application/x-www-form-urlencoded")
	public ResponseEntity<String> processOpened(@RequestParam(value="mandrill_events") Events events, HttpServletRequest request) 
	{
		boolean validSignature = mandrillSignatureValidationService.verifySignature(request, 
																	mandrillKeyService.getProperty(AuthKeyPropertyKey.MANDRILL_AUTH_KEY_OPENED),
																	mandrillKeyService.getProperty(WebhookUrlPropertyKey.MANDRILL_WEBHOOK_URL_OPENED));
		
		if(validSignature)
			return super.processOpened(events, request);
		else
		{
			logAuthWarning(request.getContextPath()+request.getServletPath());
			return new ResponseEntity<String>(HttpStatus.UNAUTHORIZED);
		}
	}
	
	/**
	 * Handles the Mandrill Spam Webhook callback with signature authentication.  
	 * @param events data instantiated from parsed JSON of Mandrill callback.
	 * @param request
	 * @return HttpStatus.OK if the X-Mandrill-Signature header matches the expected value.  If not
	 * HttpStatus.UNAUTHORIZED is returned.
	 */
	@Override
	@RequestMapping(value="/spam",method={RequestMethod.POST},consumes="application/x-www-form-urlencoded")
	public ResponseEntity<String> processSpam(@RequestParam(value="mandrill_events") Events events, HttpServletRequest request) 
	{
		boolean validSignature = mandrillSignatureValidationService.verifySignature(request, 
																	mandrillKeyService.getProperty(AuthKeyPropertyKey.MANDRILL_AUTH_KEY_SPAM),
																	mandrillKeyService.getProperty(WebhookUrlPropertyKey.MANDRILL_WEBHOOK_URL_SPAM));
		
		if(validSignature)
			return super.processSpam(events, request);
		else
		{
			logAuthWarning(request.getContextPath()+request.getServletPath());
			return new ResponseEntity<String>(HttpStatus.UNAUTHORIZED);
		}
	}
	

	/**
	 * Handles the Mandrill Rejected Webhook callback with signature authentication.  
	 * @param events data instantiated from parsed JSON of Mandrill callback.
	 * @param request
	 * @return HttpStatus.OK if the X-Mandrill-Signature header matches the expected value.  If not
	 * HttpStatus.UNAUTHORIZED is returned.
	 */
	@Override
	@RequestMapping(value="/rejected",method={RequestMethod.POST},consumes="application/x-www-form-urlencoded")
	public ResponseEntity<String> processRejected(@RequestParam(value="mandrill_events") Events events, HttpServletRequest request) 
	{
		boolean validSignature = mandrillSignatureValidationService.verifySignature(request, 
																	mandrillKeyService.getProperty(AuthKeyPropertyKey.MANDRILL_AUTH_KEY_REJECTED),
																	mandrillKeyService.getProperty(WebhookUrlPropertyKey.MANDRILL_WEBHOOK_URL_REJECTED));
		
		if(validSignature)
			return super.processRejected(events, request);
		else
		{
			logAuthWarning(request.getContextPath()+request.getServletPath());
			return new ResponseEntity<String>(HttpStatus.UNAUTHORIZED);
		}
	}
	

	
	/**
	 * Handles the Mandrill Delayed Webhook callback with signature authentication.  
	 * @param events data instantiated from parsed JSON of Mandrill callback.
	 * @param request
	 * @return HttpStatus.OK if the X-Mandrill-Signature header matches the expected value.  If not
	 * HttpStatus.UNAUTHORIZED is returned.
	 */
	@Override
	@RequestMapping(value="/delayed",method={RequestMethod.POST},consumes="application/x-www-form-urlencoded")
	public ResponseEntity<String> processDelayed(@RequestParam(value="mandrill_events") Events events, HttpServletRequest request) 
	{
		boolean validSignature = mandrillSignatureValidationService.verifySignature(request, 
																	mandrillKeyService.getProperty(AuthKeyPropertyKey.MANDRILL_AUTH_KEY_DELAYED),
																	mandrillKeyService.getProperty(WebhookUrlPropertyKey.MANDRILL_WEBHOOK_URL_DELAYED));
		
		if(validSignature)
			return super.processDelayed(events, request);
		else
		{
			logAuthWarning(request.getContextPath()+request.getServletPath());
			return new ResponseEntity<String>(HttpStatus.UNAUTHORIZED);
		}
	}


	
	/**
	 * Handles the Mandrill Soft Bounce Webhook callback with signature authentication.  
	 * @param events data instantiated from parsed JSON of Mandrill callback.
	 * @param request
	 * @return HttpStatus.OK if the X-Mandrill-Signature header matches the expected value.  If not
	 * HttpStatus.UNAUTHORIZED is returned.
	 */
	@Override
	@RequestMapping(value="/soft-bounce",method={RequestMethod.POST},consumes="application/x-www-form-urlencoded")
	public ResponseEntity<String> processSoftBounce(@RequestParam(value="mandrill_events") Events events, HttpServletRequest request) 
	{
		boolean validSignature = mandrillSignatureValidationService.verifySignature(request, 
																	mandrillKeyService.getProperty(AuthKeyPropertyKey.MANDRILL_AUTH_KEY_SOFT_BOUNCE),
																	mandrillKeyService.getProperty(WebhookUrlPropertyKey.MANDRILL_WEBHOOK_URL_SOFT_BOUNCE));
		
		if(validSignature)
			return super.processSoftBounce(events, request);
		else
		{
			logAuthWarning(request.getContextPath()+request.getServletPath());
			return new ResponseEntity<String>(HttpStatus.UNAUTHORIZED);
		}
	}
	

	
	/**
	 * Handles the Mandrill Click Webhook callback with signature authentication.  
	 * @param events data instantiated from parsed JSON of Mandrill callback.
	 * @param request
	 * @return HttpStatus.OK if the X-Mandrill-Signature header matches the expected value.  If not
	 * HttpStatus.UNAUTHORIZED is returned.
	 */
	@Override
	@RequestMapping(value="/click",method={RequestMethod.POST},consumes="application/x-www-form-urlencoded")
	public ResponseEntity<String> processClick(@RequestParam(value="mandrill_events") Events events, HttpServletRequest request) 
	{
		boolean validSignature = mandrillSignatureValidationService.verifySignature(request, 
																	mandrillKeyService.getProperty(AuthKeyPropertyKey.MANDRILL_AUTH_KEY_CLICK),
																	mandrillKeyService.getProperty(WebhookUrlPropertyKey.MANDRILL_WEBHOOK_URL_CLICK));
		
		if(validSignature)
			return super.processClick(events, request);
		else
		{
			logAuthWarning(request.getContextPath()+request.getServletPath());
			return new ResponseEntity<String>(HttpStatus.UNAUTHORIZED);
		}
	}
	
	
	/**
	 * Handles the Mandrill Unsubscribe Webhook callback with signature authentication.  
	 * @param events data instantiated from parsed JSON of Mandrill callback.
	 * @param request
	 * @return HttpStatus.OK if the X-Mandrill-Signature header matches the expected value.  If not
	 * HttpStatus.UNAUTHORIZED is returned.
	 */
	@Override
	@RequestMapping(value="/unsubscribe",method={RequestMethod.POST},consumes="application/x-www-form-urlencoded")
	public ResponseEntity<String> processUnsubscribe(@RequestParam(value="mandrill_events") Events events, HttpServletRequest request) 
	{
		boolean validSignature = mandrillSignatureValidationService.verifySignature(request, 
																	mandrillKeyService.getProperty(AuthKeyPropertyKey.MANDRILL_AUTH_KEY_UNSUBSCRIBE),
																	mandrillKeyService.getProperty(WebhookUrlPropertyKey.MANDRILL_WEBHOOK_URL_UNSUBSCRIBE));
		
		if(validSignature)
			return super.processUnsubscribe(events, request);
		else
		{
			logAuthWarning(request.getContextPath()+request.getServletPath());
			return new ResponseEntity<String>(HttpStatus.UNAUTHORIZED);
		}
	}


	/**
	 * Helper method that outputs canned authentication warning.
	 * @param requestPath
	 */
	private void logAuthWarning(String requestPath)
	{
		log.warn("Webhook request failed authorization for "+requestPath+".  Check the spring-mandrill-webhook.properties file for the correct authorization key and webhook url");
	}
}
