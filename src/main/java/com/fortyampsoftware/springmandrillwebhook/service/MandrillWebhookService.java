/**
 * Copyright (c) 2013  Forty Amp Software <http://www.40amps.com>
 *
 *  This file is part of the Spring-Mandrill-Webhooks project:
 * (https://bitbucket.org/FortyAmpSoftware/spring-mandrill-webhooks/overview)
 *
 *  Spring-Mandrill-Webhooks is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Spring-Mandrill-Webhooks is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser Public License
 *  along with Spring-Mandrill-Webhooks.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.fortyampsoftware.springmandrillwebhook.service;

import com.fortyampsoftware.springmandrillwebhook.model.mandrill.Events;

/**
 * A MandrillWebhookService is designed to handle specific Mandrill Webhook Events that have
 * been routed to it by type (Send, Hard Bounce, Click, etc.).  A MandrillWebhookService
 * will usually receive the appropriate Events via routing from a MandrillWebhookController.
 *  
 * @author D. Scott Felblinger
 */
public interface MandrillWebhookService
{
	/**
	 * Processes Mandrill Webhook Send Events
	 * @param events Send events created from parsed Mandrill JSON data. 
	 */
	void processSendEvents(Events events);
	
	/**
	 * Processes Mandrill Webhook Hard Bounce Events
	 * @param events Send events created from parsed Mandrill JSON data. 
	 */
	void processHardBounceEvents(Events events);
	
	/**
	 * Processes Mandrill Webhook Opened Events
	 * @param events Send events created from parsed Mandrill JSON data. 
	 */
	void processOpenedEvents(Events events);
	
	/**
	 * Processes Mandrill Webhook Spam Events
	 * @param events Send events created from parsed Mandrill JSON data. 
	 */
	void processSpamEvents(Events events);
	
	/**
	 * Processes Mandrill Webhook Rejected Events
	 * @param events Send events created from parsed Mandrill JSON data. 
	 */
	void processRejectedEvents(Events events);
	
	/**
	 * Processes Mandrill Webhook Delayed Events
	 * @param events Send events created from parsed Mandrill JSON data. 
	 */
	void processDelayedEvents(Events events);
	
	/**
	 * Processes Mandrill Webhook Soft Bounce Events
	 * @param events Send events created from parsed Mandrill JSON data. 
	 */
	void processSoftBounceEvents(Events events);
	
	/**
	 * Processes Mandrill Webhook Click Events
	 * @param events Send events created from parsed Mandrill JSON data. 
	 */
	void processClickEvents(Events events);
	
	/**
	 * Processes Mandrill Webhook Unsubscribe Events
	 * @param events Send events created from parsed Mandrill JSON data. 
	 */
	void processUnsubscribeEvents(Events events);
}
