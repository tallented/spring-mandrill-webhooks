package com.fortyampsoftware.springmandrillwebhook.service;

/**
 * Created with IntelliJ IDEA.
 * User: ballmw
 * Date: 11/18/13
 * Time: 10:11 AM
 * To change this template use File | Settings | File Templates.
 */
public interface MandrillKeyService {

    String getProperty(String key);
}
