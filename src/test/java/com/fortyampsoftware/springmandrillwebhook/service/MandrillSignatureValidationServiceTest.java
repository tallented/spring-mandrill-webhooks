/**
 * Copyright (c) 2013  Forty Amp Software <http://www.40amps.com>
 *
 *  This file is part of the Spring-Mandrill-Webhooks project:
 * (https://bitbucket.org/FortyAmpSoftware/spring-mandrill-webhooks/overview)
 *
 *  Spring-Mandrill-Webhooks is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Spring-Mandrill-Webhooks is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser Public License
 *  along with Spring-Mandrill-Webhooks.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.fortyampsoftware.springmandrillwebhook.service;

import static org.junit.Assert.*;

import java.util.TreeMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;


/**
 * @author D. Scott Felblinger
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class MandrillSignatureValidationServiceTest
{
	private MockHttpServletRequest request;
	private MandrillSignatureValidationService mandrillSignatureValidationService;
	private String authenticationKey = "madeUpAuthKey";
	private String badAuthenticationKey = "badKeyMessYouUp";
	private String webhookUrl = "https://bitbucket.org/FortyAmpSoftware/spring-mandrill-webhooks/overview";
	private String badWebhookUrl = "https://www.urlmessyouup.com";
	private TreeMap<String,String> parameterMap;
	
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		mandrillSignatureValidationService = new MandrillSignatureValidationService();
		MockitoAnnotations.initMocks(this);
		request = new MockHttpServletRequest();
		parameterMap = new TreeMap<String,String>();
		parameterMap.put("mandrill_events","somejsondatafortesting");
		request.setParameters(parameterMap);
		request.addHeader("X-Mandrill-Signature", "GqiOjNqbdbwlP29b+S40SijG2Bs=");
	}

	/**
	 * Test method for {@link com.fortyampsoftware.springmandrillwebhook.service.MandrillSignatureValidationService#verifySignature(javax.servlet.http.HttpServletRequest, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testVerifySignatureOk()
	{
		assertTrue(mandrillSignatureValidationService.verifySignature(request, authenticationKey, webhookUrl));
	}
	
	@Test
	public void testVerifySignatureBadKey()
	{
		assertFalse(mandrillSignatureValidationService.verifySignature(request, badAuthenticationKey, webhookUrl));
	}
	
	@Test
	public void testVerifySignatureBadUrl()
	{
		assertFalse(mandrillSignatureValidationService.verifySignature(request, authenticationKey, badWebhookUrl));
	}

}
